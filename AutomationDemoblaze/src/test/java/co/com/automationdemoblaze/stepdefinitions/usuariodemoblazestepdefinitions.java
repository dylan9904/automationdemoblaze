package co.com.automationdemoblaze.stepdefinitions;

import co.com.automationdemoblaze.questions.ValidacionCompra;
import co.com.automationdemoblaze.questions.ValidacionInicioSesion;
import co.com.automationdemoblaze.questions.ValidacionProductos;
import co.com.automationdemoblaze.tasks.ComprarLaptop;
import co.com.automationdemoblaze.tasks.InicioSesionCorrecto;
import co.com.automationdemoblaze.tasks.RegistroCorrecto;
import co.com.automationdemoblaze.tasks.VerPrecioNombreDescripcion;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class usuariodemoblazestepdefinitions {
    @Managed(driver = "chrome")
    WebDriver hisdriver;
    @Before
    public void setThestago() {
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled("Dylan");
    }
    @Given("^User in the homepage$")
    public void userInTheHomepage() {
        theActorInTheSpotlight().wasAbleTo(Open.url("https://www.demoblaze.com"));
    }

    @When("^El usuario ingresa los campos para el registro$")
    public void elUsuarioIngresaLosCamposParaElRegistro() {
        theActorInTheSpotlight().attemptsTo(RegistroCorrecto.registroCorrecto());
    }


    @When("^El usuario inicia sesion correctamente$")
    public void elUsuarioIniciaSesionCorrectamente() {
        theActorInTheSpotlight().attemptsTo(InicioSesionCorrecto.inicioSesionCorrecto());
    }

    @Then("^puede ver nombre de usuario$")
    public void puedeVerNombreDeUsuario() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ValidacionInicioSesion.VerifyUsername()));
    }

    @When("^El usuario inicia sesion correctamente se dirige a la seccion de laptops$")
    public void elUsuarioIniciaSesionCorrectamenteSeDirigeALaSeccionDeLaptops() {
        theActorInTheSpotlight().attemptsTo(VerPrecioNombreDescripcion.verPrecioNombreDescripcion());
    }

    @Then("^El usuario puede ver nombre descripcion precio$")
    public void elUsuarioPuedeVerNombreDescripcionPrecio() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ValidacionProductos.VerifyProducts(), Matchers.is(true)));
    }

    @When("^El usuario inicia sesion correctamente se dirige a la seccion de laptops y compra una$")
    public void elUsuarioIniciaSesionCorrectamenteSeDirigeALaSeccionDeLaptopsYCompraUna() {
            theActorInTheSpotlight().attemptsTo(ComprarLaptop.comprarLaptop());
    }

    @Then("^El usuario puede ver un mensaje despues de confirmar su compra$")
    public void elUsuarioPuedeVerUnMensajeDespuesDeConfirmarSuCompra() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ValidacionCompra.VerifyMessage(), Matchers.is(true)));
    }

}
