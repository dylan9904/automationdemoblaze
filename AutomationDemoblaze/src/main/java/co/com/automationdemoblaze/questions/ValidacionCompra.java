package co.com.automationdemoblaze.questions;

import co.com.automationdemoblaze.userinterfaces.PantallaCompra;
import co.com.automationdemoblaze.userinterfaces.PantallaPrincipal;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import org.assertj.core.api.SoftAssertions;

public class ValidacionCompra implements Question<Boolean> {
    private final SoftAssertions validate = new SoftAssertions();
    @Override
    public Boolean answeredBy(Actor actor) {
        validate.assertThat(PantallaCompra.MESSAGE.resolveFor(actor).getText()).isEqualTo("Thank you for your purchase!");
        return true;
    }
    public static Question <Boolean> VerifyMessage() {
        return new ValidacionCompra();
    }

}
