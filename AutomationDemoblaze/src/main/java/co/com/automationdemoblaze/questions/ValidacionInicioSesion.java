package co.com.automationdemoblaze.questions;

import co.com.automationdemoblaze.userinterfaces.PantallaCompra;
import co.com.automationdemoblaze.userinterfaces.PantallaPrincipal;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import org.assertj.core.api.SoftAssertions;

public class ValidacionInicioSesion implements Question<Boolean> {
    private final SoftAssertions validate = new SoftAssertions();
    @Override
    public Boolean answeredBy(Actor actor) {
        validate.assertThat(PantallaPrincipal.NAMEUSER.resolveFor(actor).getText()).isEqualTo("Welcome dylan9904");
        return true;
    }
    public static Question <Boolean> VerifyUsername() {
        return new ValidacionInicioSesion();
    }

}

