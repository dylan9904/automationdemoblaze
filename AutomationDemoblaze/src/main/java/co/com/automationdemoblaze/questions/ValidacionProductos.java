package co.com.automationdemoblaze.questions;

import co.com.automationdemoblaze.userinterfaces.PantallaPrincipal;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import org.assertj.core.api.SoftAssertions;

public class ValidacionProductos implements Question<Boolean> {
    private final SoftAssertions validate = new SoftAssertions();
    @Override
    public Boolean answeredBy(Actor actor) {
        validate.assertThat(PantallaPrincipal.LAPTOP_NAME.resolveFor(actor).getText()).isEqualTo("MacBook air");
        validate.assertThat(PantallaPrincipal.PRICE.resolveFor(actor).getText()).isEqualTo("$700 *includes tax");
        validate.assertThat(PantallaPrincipal.DESCRIPTION.resolveFor(actor).getText()).isEqualTo("Product description");
        validate.assertAll();
        return true;
    }
    public static Question <Boolean> VerifyProducts() {
        return new ValidacionProductos();
    }

}