package co.com.automationdemoblaze.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Espera implements Interaction {
    private int waitValue;
    public Espera(int waitValue){this.waitValue = waitValue;}
    @Override
    public <T extends Actor> void performAs(T actor) {
        try {
            Thread.sleep(waitValue * 1000);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }
    public static Espera theSeconds(int waitValue){
        return instrumented(Espera.class,waitValue);
    }
}
