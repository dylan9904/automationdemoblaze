package co.com.automationdemoblaze.tasks;

import co.com.automationdemoblaze.interactions.AceptarVentanaEmergente;
import co.com.automationdemoblaze.interactions.Espera;
import co.com.automationdemoblaze.userinterfaces.PantallaPrincipal;
import co.com.automationdemoblaze.userinterfaces.PantallaRegistro;
import co.com.automationdemoblaze.utils.Constantes;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;

import java.util.Date;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class RegistroCorrecto implements Task {
    String code = Long.toString(new Date().getTime()).substring(4,10);
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Espera.theSeconds(4),
                Click.on(PantallaPrincipal.SIGN_UP),
                SendKeys.of(Constantes.USUARIO+code).into(PantallaRegistro.USERNAME),
                SendKeys.of(Constantes.CONTRASENA).into(PantallaRegistro.PASSWORD),
                Click.on(PantallaRegistro.SIGN_UP_BUTTON),
                Espera.theSeconds(4),
                AceptarVentanaEmergente.aceptarVentanaEmergente()
        );
        actor.remember("1",code);
    }
    public static RegistroCorrecto registroCorrecto(){
        return instrumented(RegistroCorrecto.class);
    }
}
