package co.com.automationdemoblaze.tasks;

import co.com.automationdemoblaze.interactions.Espera;
import co.com.automationdemoblaze.userinterfaces.PantallaPrincipal;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class VerPrecioNombreDescripcion implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                InicioSesionCorrecto.inicioSesionCorrecto(),
                Espera.theSeconds(4),
                Click.on(PantallaPrincipal.LAPTOPS),
                Click.on(PantallaPrincipal.LAPTOP)
        );
    }
    public static VerPrecioNombreDescripcion verPrecioNombreDescripcion(){
        return instrumented(VerPrecioNombreDescripcion.class);
    }
}
