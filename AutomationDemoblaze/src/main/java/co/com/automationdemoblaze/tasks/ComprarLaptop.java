package co.com.automationdemoblaze.tasks;

import co.com.automationdemoblaze.interactions.AceptarVentanaEmergente;
import co.com.automationdemoblaze.interactions.Espera;
import co.com.automationdemoblaze.userinterfaces.PantallaCompra;
import co.com.automationdemoblaze.userinterfaces.PantallaPrincipal;
import co.com.automationdemoblaze.utils.Constantes;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;
import net.serenitybdd.screenplay.waits.Wait;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class ComprarLaptop implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                InicioSesionCorrecto.inicioSesionCorrecto(),
                Espera.theSeconds(4),
                Click.on(PantallaPrincipal.LAPTOPS),
                Click.on(PantallaCompra.LAPTOP),
                Click.on(PantallaCompra.ADD_CART),
                Espera.theSeconds(4),
                AceptarVentanaEmergente.aceptarVentanaEmergente(),
                Click.on(PantallaPrincipal.CART),
                Click.on(PantallaCompra.PLACE_ORDER),
                SendKeys.of(Constantes.NOMBRE).into(PantallaCompra.NAME),
                SendKeys.of(Constantes.PAIS).into(PantallaCompra.COUNTRY),
                SendKeys.of(Constantes.CIUDAD).into(PantallaCompra.CITY),
                SendKeys.of(Constantes.TARJETA).into(PantallaCompra.CREDIT_CARD),
                SendKeys.of(Constantes.MES).into(PantallaCompra.MONTH),
                SendKeys.of(Constantes.YEAR).into(PantallaCompra.YEAR),
                Click.on(PantallaCompra.PURCHASE)
        );
    }
    public static ComprarLaptop comprarLaptop(){
        return instrumented(ComprarLaptop.class);
    }
}
