package co.com.automationdemoblaze.tasks;

import co.com.automationdemoblaze.interactions.AceptarVentanaEmergente;
import co.com.automationdemoblaze.interactions.Espera;
import co.com.automationdemoblaze.userinterfaces.PantallaInicioSesion;
import co.com.automationdemoblaze.userinterfaces.PantallaPrincipal;
import co.com.automationdemoblaze.userinterfaces.PantallaRegistro;
import co.com.automationdemoblaze.utils.Constantes;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;

import java.util.Date;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class InicioSesionCorrecto implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
                actor.attemptsTo(
                Espera.theSeconds(4),
                Click.on(PantallaPrincipal.LOG_IN),
                SendKeys.of(Constantes.USUARIO).into(PantallaInicioSesion.USERNAME),
                SendKeys.of(Constantes.CONTRASENA).into(PantallaInicioSesion.PASSWORD),
                Click.on(PantallaInicioSesion.LOGIN_BUTTON)
        );
    }
    public static InicioSesionCorrecto inicioSesionCorrecto(){
        return instrumented(InicioSesionCorrecto.class);
    }
}
