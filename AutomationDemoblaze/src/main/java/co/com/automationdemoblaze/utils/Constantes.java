package co.com.automationdemoblaze.utils;

public class Constantes {
    public static final String USUARIO = "dylan1999";
    public static final String USUARIO_INCORRECTO = "dylan088088";
    public static final String CONTRASENA = "1234";
    public static final String NOMBRE = "Dylan";
    public static final String PAIS = "Colombia";
    public static final String CIUDAD = "Medellin";
    public static final String TARJETA = "258479";
    public static final String MES = "12";
    public static final String YEAR = "2019";
}
