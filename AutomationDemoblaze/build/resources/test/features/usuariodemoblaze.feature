@tag
Feature: El usuario ingresa a la pagina demoblaze para realizar compra

  Background:
    Given User in the homepage
  @registrousuario
  Scenario: El usuario se registra en la pagina
    When El usuario ingresa los campos para el registro

  @iniciosesioncorrecto
  Scenario: El usuario inicia sesion correctamente y observa los datos de productos
    When El usuario inicia sesion correctamente
    Then puede ver nombre de usuario

  @comparalaptops
  Scenario: El usuario inicia sesion correctamente y compara una laptop
    When El usuario inicia sesion correctamente se dirige a la seccion de laptops
    Then El usuario puede ver nombre descripcion precio

  @compralaptops
  Scenario: El usuario inicia sesion correctamente y compra una laptop
    When El usuario inicia sesion correctamente se dirige a la seccion de laptops y compra una
    Then El usuario puede ver un mensaje despues de confirmar su compra